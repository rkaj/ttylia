# ttylia - Julia fractals on tty

This is intended to write the output to a DIGITAL VT220 terminal.

```
sudo chown $USER /dev/ttyUSB0
stty -F /dev/ttyUSB0 raw 19200
cargo run > /dev/ttyUSB0
```

Note: 19200 bits per second gives about 2 frames per second (a frame
is 40x24 chars, a char is 8 data bits + start and stop bit, so 10
bits).
