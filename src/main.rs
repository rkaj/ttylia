use num_complex::Complex32;
use std::{io::{self, Write}, thread::sleep, time::{Duration, Instant}};

const CHARS: &[u8; 13] = b"\xA1\xA2\xA3\xA4\xA5\xA6\xA7\xA8\xA9\xA9\xA9\xA9\xA9";

const CSI: u8 = b'\x9b'; // Start of control sequence, equivalent to "\x1B["

fn csi(code: &[u8]) -> Vec<u8> {
    let mut res = vec![CSI]; // or vec![0x1B, b'['] for 7-bit compat.
    res.extend(code);
    res
}

fn main() -> Result<(), io::Error> {
    const WIDTH: u16 = 40;
    const HEIGHT: u16 = 24;
    const SY: f32 = 3.2 / (HEIGHT as f32);
    const SX: f32 = SY / 1.4;
    const X0: f32 = ((WIDTH / 2) as f32) * SX;
    const Y0: f32 = ((HEIGHT / 2) as f32) * SY;
    let frame_time = Duration::from_millis(300 /*1000/30*/);
    let max_iter = u8::try_from(CHARS.len()-1).unwrap();
    let mut buffer = [0u8; (WIDTH * HEIGHT) as usize];
    let mut c = Complex32 { re: -0.5, im: 0.5 };
    let mut rotor = Complex32 { re: 1., im: frame_time.as_secs_f32() * 0.2 };
    let mut frame = 0;
    let mut out  = io::stdout().lock();

    out.write_all(&csi(b"2J"))?;
    out.write_all(&csi(b"?25l"))?;
    out.write_all(b"\x1B)@\x1B~")?; // Enable custom character set
    define_pixels(&mut out);
    //out.write_all(b"\r\n\x1B#6 Hello \xA0\xA1\xA2\xA3\xA4\xA5\xA6 World\r\n").unwrap();
    //return;

    loop {
        let frame_start = Instant::now();
        c *= rotor;
        /*eprintln!("{:.4} / {:+.4} {:+.4} / {:+.4}",
                  c.l1_norm(),
                  (c.l1_norm() - 1.),
                  (rotor.l1_norm() - 1.),
                  (c.l1_norm() - 1.) * (rotor.l1_norm() - 1.));*/
        if c.norm_sqr() > 2. {
            rotor *= (1. / rotor.norm_sqr()).sqrt();
        }
        frame += 1;
        let mut p = 0;
        for y in 0..HEIGHT / 2 {
            let zy = f32::from(y) * SY - Y0;
            for x in 0..WIDTH {
                let z = Complex32 {
                    re: f32::from(x) * SX - X0,
                    im: zy,
                };
                let val = pixel(julia(z, c, max_iter));
                buffer[p] = val;
                p += 1;
                buffer[buffer.len() - p] = val;
            }
        }
        let status = format!("ttylia | Rasmus Kaj | Rust | {:.3}", c.l1_norm());
        buffer[0..status.len()].clone_from_slice(status.as_bytes());
        //buffer[0] = b't';
        out.write_all(b"\x1B[H").unwrap();
        for y in 0..HEIGHT {
            out.write_all(b"\x1B#6").unwrap();
            out.write_all(&buffer[usize::from(y * WIDTH)..usize::from((y + 1) * WIDTH)]).unwrap();
        }
        out.write_all(b"\x1B#6").unwrap();
        //out.write_all(b"\r").unwrap();
        //out.write_all(b"\x1B[Httylia | Rasmus Kaj | Rust \n").unwrap();
        //writeln!(out, "| {c:+.5} | {frame:4} ").unwrap();
        out.flush().unwrap();
        if let Some(remains) = frame_time.checked_sub(frame_start.elapsed()) {
            //writeln!(out, " {remains:.3?} ").unwrap();
            sleep(remains);
        //} else {
        //    writeln!(out, "Hurry!").unwrap();
        }
    }
}

fn define_pixels(out: &mut impl Write) {
    let chars = [
        chardef([
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b1111_1110,
            0b0000_0000,
        ]),
        chardef([
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
        ]),
        chardef([ // 1 pixel
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0001_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
        ]),
        chardef([ // 4 pixels
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0010_1000,
            0b0000_0000,
            0b0010_1000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
        ]),
        chardef([ // 5 pixels
            0b0000_0000,
            0b0000_0000,
            0b0100_0100,
            0b0000_0000,
            0b0001_0000,
            0b0000_0000,
            0b0100_0100,
            0b0000_0000,
            0b0000_0000,
            0b0000_0000,
        ]),
        chardef([ // 6 pixels
            0b0000_0000,
            0b0100_1000,
            0b0000_0000,
            0b0000_0000,
            0b0100_1000,
            0b0000_0000,
            0b0000_0000,
            0b0100_1000,
            0b0000_0000,
            0b0000_0000,
        ]),
        chardef([ // 9 pixels
            0b0000_0010,
            0b0001_0000,
            0b1000_0000,
            0b0000_0010,
            0b0001_0000,
            0b1000_0000,
            0b0000_0010,
            0b0001_0000,
            0b1000_0000,
            0b0000_0000,
        ]),
        chardef([ // half
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0000_0000,
        ]),
        chardef([
            0b1010_1010,
            0b0111_0100,
            0b1010_1010,
            0b0101_1100,
            0b1110_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_1100,
            0b1010_1010,
            0b0000_0000,
        ]),
        chardef([
            0b1110_1110,
            0b0111_0110,
            0b1011_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0101_0100,
            0b1010_1010,
            0b0000_0000,
        ]),
    ];
    //eprintln!("Defining {} chars", chars.len());
    //eprintln!("Def: {chars:?}");
    out.write_all(b"\x1BP1;1;1{@").unwrap();
    if let Some((first, rest)) = chars.split_first() {
        out.write_all(first).unwrap();
        for ch in rest {
            out.write_all(b";").unwrap();
            out.write_all(ch).unwrap();
        }
    }
    out.write_all(b"\x1B\\").unwrap();
}

fn chardef(bits: [u8; 10]) -> [u8;17] {
    let mut result = [0;17];
    for y in 0..6 {
        for x in 0..8 {
            if bits[y] & (0b10000000 >> x) > 0 {
                result[x] |= 1 << y;
            }
        }
    }
    for y in 0..4 {
        for x in 0..8 {
            if bits[y+6] & (0b10000000 >> x) > 0 {
                result[9 + x] |= 1 << y;
            }
        }
    }
    for c in &mut result {
        *c += b'?';
    }
    result[8] = b'/';
    //eprintln!("chardef2: {}", String::from_utf8_lossy(&result));
    result
}


fn pixel(i: u8) -> u8 {
    CHARS[usize::from(i)]
}

fn julia(z: Complex32, c: Complex32, max_i: u8) -> u8 {
    let mut z = z;
    for i in 1..max_i {
        if z.norm_sqr() > 4.0 {
            return i;
        }
        z = z * z + c;
    }
    if z.norm_sqr() > 4.0 {
        max_i
    } else {
        0
    }
}
